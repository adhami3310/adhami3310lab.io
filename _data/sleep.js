import { fitness } from "@googleapis/fitness";
import { auth } from "google-auth-library";

async function loadSavedCredentialsIfExist() {
  try {
    const client = auth.fromJSON({
      type: "authorized_user",
      client_id:
        "95378323537-uilfoks0219a6qjmk7ipcl8sl6o1q16j.apps.googleusercontent.com",
      client_secret: process.env.CLIENT_SECRET,
      refresh_token: process.env.REFRESH_TOKEN,
    });
    if ("refreshAccessToken" in client) {
      return client; // this is definitely an `OAuth2Client`
    }
    return null;
  } catch (err) {
    return null;
  }
}

async function getSleepData(auth) {
  const fit = fitness({ version: "v1", auth });
  const res = await fit.users.sessions.list({
    startTime: "2024-02-10T00:00:00-00:00",
    userId: "me",
    activityType: [72],
  });
  return res.data.session?.map((i) => ({
    start: new Date(Number(i.startTimeMillis)),
    end: new Date(Number(i.endTimeMillis)),
  }));
}

export default async function () {
  const client = await loadSavedCredentialsIfExist();
  const sleepData = client ? await getSleepData(client) : undefined;
  return sleepData;
}
