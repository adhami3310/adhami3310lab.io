export default function (eleventyConfig) {
  eleventyConfig.addAsyncShortcode("sleepDisplay", async function (sleepData) {
    /**
     * @param {Date} date
     */
    function timeOfDate(date) {
      return (date.getUTCHours() + date.getUTCMinutes() / 60) / 24;
    }

    if (!sleepData) return "";
    /** @type {{start: Date, end: Date}[]} */
    const allSleepSessions = sleepData;
    /** @type {{start:number, end:number}[][]} */
    const timesOfDay = [];
    for (let offset = 0; offset < 30; offset += 1) {
      let date = new Date(Date.now() - offset * 24 * 60 * 60 * 1000);

      function matches(anotherDate) {
        return (
          date.getUTCDate() == anotherDate.getUTCDate() &&
          date.getUTCMonth() == anotherDate.getUTCMonth() &&
          date.getUTCFullYear() == anotherDate.getUTCFullYear()
        );
      }

      /** @type {number[]} */
      const sleepSessions = [];
      for (const sleepSession of allSleepSessions) {
        if (
          !matches(sleepSession.start, date) &&
          !matches(sleepSession.end, date)
        )
          continue;
        if (
          matches(sleepSession.start, date) &&
          matches(sleepSession.end, date)
        ) {
          sleepSessions.push({
            start: timeOfDate(sleepSession.start),
            end: timeOfDate(sleepSession.end),
          });
        } else if (matches(sleepSession.start, date)) {
          sleepSessions.push({
            start: timeOfDate(sleepSession.start),
            end: 1,
          });
        } else {
          sleepSessions.push({
            start: 0,
            end: timeOfDate(sleepSession.end),
          });
        }
      }
      timesOfDay.push(sleepSessions);
    }
    timesOfDay.reverse();

    const sum =
      timesOfDay.reduce(
        (c, d) => c + d.reduce((a, b) => a + (b.end - b.start), 0),
        0
      ) / 30;

    function formatTime(n) {
      return (
        Math.floor(((n + 1) * 24) % 24)
          .toString()
          .padStart(2, "0") +
        ":" +
        (Math.floor((n + 1) * 24 * 60) % 60).toString().padStart(2, "0")
      );
    }

    return `<h2>Sleep Schedule in UTC <span class="midnight-key">(Midnight in PT)</span></h2><div id="sleep-container">${timesOfDay
      .map(
        (d) =>
          `<div class="day"><div class="midnight"></div>${d
            .map(
              (s) =>
                `<div class="session" style="height:${
                  (s.end - s.start) * 100
                }%; top: ${s.start * 100}%" title="${formatTime(
                  s.start
                )} to ${formatTime(s.end)} (${formatTime(
                  s.start - 4 / 24
                )} to ${formatTime(s.end - 4 / 24)} Eastern Time)" ></div>`
            )
            .join("")}</div>`
      )
      .join(
        ""
      )}</div><div id="time-span"><div>30 Days Ago</div><div class="dash"></div><div>${(100 - sum * 100).toFixed(2)}% uptime</div><div class="dash"></div><div>Today</div></div>`;
  });

  eleventyConfig.addAsyncShortcode("year", async () => {
    return new Date().getUTCFullYear().toString();
  });

  eleventyConfig.addAsyncShortcode("age", async () => {
    return `<h2>Age</h2><div id="age"></div>
    <script type="module">
        const born = new Date(Date.UTC(2002, 11, 30, 9, 0, 0, 0));
        const year = 1000 * 60 * 60 * 24 * 3652425 / 10000;
        const f = () => {
          const now = new Date();
          document.getElementById("age").innerHTML = "<div id='year'>" + Math.floor((now-born) / year) + "</div>" +
            "<div id='remainder'>." + Math.floor(((now - born) % year * 100000000000) / year ).toString().padStart(11, '0') + "</div>";
          window.requestAnimationFrame(f);
        };
        window.requestAnimationFrame(f);
      </script>`;
  });

  eleventyConfig.addPassthroughCopy("images");
  eleventyConfig.addPassthroughCopy("style.css");
  eleventyConfig.addPassthroughCopy(".well-known");
  eleventyConfig.addPassthroughCopy("robots.txt");
  eleventyConfig.addPassthroughCopy("sitemap.xml");
  eleventyConfig.addPassthroughCopy("*.pdf");
}
